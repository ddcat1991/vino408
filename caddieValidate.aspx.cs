﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class caddieValidate : System.Web.UI.Page
{
    double horstotal = 0;
    const double TVA = 0.196;
    const double E_TO_F = 6.55957;
    public CultureInfo cInfo
    {
        get;
        set;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Validate_Btn_Click(object sender, EventArgs e)
    {
        //TODO:send e-mail
        sendEmail();
        //update database Commande
        dsCommandes.InsertParameters["date"].DefaultValue = DateTime.Now.ToString();
        dsCommandes.Insert();
        //clean Session
        Session.Remove("idcomm");
        Session.Remove("total");
        //Server.Transfer("Validate.aspx");
        DataList1.Visible = false;
        validationInfo.Visible = true;
        Valide_Btn.Visible = false;

    }

    public double getUnitPrix(double unProdcomm)
    {
        this.horstotal += unProdcomm;
        return unProdcomm;
    }

    public double getHorsTotal()
    {
        return horstotal;
    }

    public double getTva()
    {
        return TVA * 100;
    }

    public double getTotalTva()
    {
        return TVA * this.horstotal;
    }

    public double getTotal()
    {
        double total = this.horstotal + this.getTotalTva();
        Session["comm_total"] = total.ToString();
        return total;
    }
    public double euroToFranc(double euro)
    {
        return euro * E_TO_F;
    }

    private void sendEmail()
    {
        /*
        SmtpClient smtpClient = new SmtpClient("z.telecom-bretagne.eu", 465);

        smtpClient.Credentials = new System.Net.NetworkCredential("jue.wang@telecom-bretagne.eu", "E822Eca3");
        smtpClient.UseDefaultCredentials = true;
        smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
        smtpClient.EnableSsl = true;
        MailMessage mail = new MailMessage();

        //Setting From , To and CC
        mail.From = new MailAddress("jue.wang@telecom-bretagne.eu", "MyWeb Site");
        mail.To.Add(new MailAddress("jue.wang@telecom-bretagne.eu"));
        //mail.CC.Add(new MailAddress("MyEmailID@gmail.com"));

        smtpClient.Send(mail); */
    }
}