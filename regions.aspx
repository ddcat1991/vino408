﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageIndex.master" AutoEventWireup="true" CodeFile="regions.aspx.cs" Inherits="regions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Nav" Runat="Server">
        <div class="header">
        </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="QuickLink" runat="server" >
    <div></div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Main" Runat="Server"  >
    <h2>Bienvenue au site Web de la société La Vinothèque, fondée en 1962.</h2>
    <h2>Nous vous proposons des vins de qualité provenant de 10 régions françaises:</h2>

    <ol>
        <li>Alsace</li>
        <li>Beaujolais</li>
        <li>Bordeaux</li>
        <li>Bourgogne</li>
        <li>Champagne</li>
        <li>Languedoc-Roussillon</li>
        <li>Médoc</li>
        <li>Provence</li>
        <li>Savoie</li>
        <li>Val de Loire</li>
    </ol>
    <br />
  
    <div style="text-align:center">
        <asp:Button runat="server"  Text="Voir notre catalogue" 
            OnClick="Unnamed2_Click"/>
    <input id="btnCatalogue" type="button" value="Voir notre catalogue" onclick="location.href='vins.aspx'" />
    </div>

    </asp:Content>

