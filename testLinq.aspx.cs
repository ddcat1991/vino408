﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class testLinq : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        CommandeDataContext cdc = new CommandeDataContext();

        if (!IsPostBack)
        {
            var clients = from client in cdc.clients
                          select client;

            DropdClient.Items.Add(new ListItem("Show All Commandes", "All"));

            foreach (client c in clients)
            {
                DropdClient.Items.Add(new ListItem(c.login, c.idclient.ToString()));
            }
            var persoCommand = from commande in cdc.commandes
                               join aclient in cdc.clients on commande.idclient equals aclient.idclient
                               select new { CommandeId = commande.idcomm, ClientLogin = aclient.login, CommandeDate = commande.date, CommandeTotal = commande.total };
            GridView1.DataSource = persoCommand;
            DataBind();
        }
        else {
            int clientid;         
            string clientid_s = DropdClient.SelectedValue;
            if (clientid_s.Equals("All"))
            {
                var persoCommand = from commande in cdc.commandes
                                   join aclient in cdc.clients on commande.idclient equals aclient.idclient
                                   select new { CommandeId = commande.idcomm, ClientLogin = aclient.login, CommandeDate = commande.date, CommandeTotal = commande.total };
                GridView1.DataSource = persoCommand;
            }
                
            else
            {
                clientid = Int32.Parse(clientid_s);
                var persoCommand = from commande in cdc.commandes
                                   where commande.idclient == clientid
                                   join aclient in cdc.clients on commande.idclient equals aclient.idclient
                                   select new {CommandeId = commande.idcomm, ClientLogin= aclient.login, CommandeDate = commande.date, CommandeTotal = commande.total };
                GridView1.DataSource = persoCommand;
            }                     
            DataBind();
        }

    }


}