﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="register.aspx.cs" Inherits="register" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table>
                <tr>
                    <td>Login:</td>
                    <td>
                        <asp:TextBox ID="tbLogin" runat="server"></asp:TextBox>       
                        <asp:RequiredFieldValidator ID="tbLoginValidator1" runat="server" ControlToValidate="tbLogin" >Login is required</asp:RequiredFieldValidator>
                        
                    </td>
                </tr>
                <tr>
                    <td>Password:</td>
                    <td>
                        <asp:TextBox ID="tbMdp" runat="server" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="tbMdpValidator" runat="server" ControlToValidate="tbMdp" >Password is required</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>Password:</td>
                    <td>
                        <asp:TextBox ID="tbConfirmMdp" runat="server" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="tbConfirmMdpValidatorRequired" runat="server" ControlToValidate="tbConfirmMdp" >Password is required</asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="tbConfirmMdpValidator" runat="server" 
                                ControlToValidate="tbConfirmMdp"
                                CssClass="ValidationError"
                                ControlToCompare="tbMdp"
                                Type="String"                                   
                                ErrorMessage="No Match" 
                                ToolTip="Password must be the same" />
                    </td>
                </tr>
                <tr>
                    <td>Nom:</td>
                    <td>
                        <asp:TextBox ID="tbNom" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="tbNomValidator" runat="server" ControlToValidate="tbNom" >Nom is required</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>Addresse:</td>
                    <td>
                        <asp:TextBox ID="tbAdresse" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Ville:</td>
                    <td>
                        <asp:TextBox ID="tbVille" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Pays:</td>
                    <td>
                        <asp:TextBox ID="tbPays" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Telephone:</td>
                    <td>
                        <asp:TextBox ID="tbTelephone" runat="server" TextMode="Phone"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Email:</td>
                    <td>
                        <asp:TextBox ID="tbEmail" runat="server" TextMode="Email"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button ID="Button1" runat="server" Text="Sign up" OnClick="Signup_Click" />
                    </td>
                </tr>
            </table>
            <asp:Label ID="regInfo" runat="server" Text=""></asp:Label>
            <br />

            <asp:SqlDataSource ID="dsClient" runat="server"
                ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                InsertCommand="INSERT INTO clients(login, mdp, nom, adresse, ville, pays, telephone, email) VALUES (@login, @mdp, @nom, @adresse, @ville, @pays, @telephone, @email)"
                SelectCommand="SELECT idclient FROM clients WHERE (login = @login)">
                <InsertParameters>
                    <asp:ControlParameter Name="login" ControlID="tbLogin" PropertyName="Text" />
                    <asp:ControlParameter Name="mdp" ControlID="tbMdp" PropertyName="Text" />
                    <asp:ControlParameter Name="nom" ControlID="tbNom"  PropertyName="Text" />
                    <asp:ControlParameter Name="adresse" ControlID="tbAdresse" PropertyName="Text" />
                    <asp:ControlParameter Name="ville" ControlID="tbVille" PropertyName="Text" />
                    <asp:ControlParameter Name="pays" ControlID="tbPays" PropertyName="Text" />
                    <asp:ControlParameter Name="telephone" ControlID="tbTelephone" PropertyName="Text" />
                    <asp:ControlParameter Name="email" ControlID="tbEmail" PropertyName="Text" />
                </InsertParameters>
                <SelectParameters>
                    <asp:ControlParameter ControlID="tbLogin" Name="login" PropertyName="Text" />
                </SelectParameters>
            </asp:SqlDataSource>

        </div>
    </form>
</body>
</html>
