﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["nom"] == null)
        {
            lbNom.Visible = false;
            lbLogout.Visible = false;
        }
        else
        {
            hlLogin.Visible = false;
            hlRegist.Visible = false;
        }
    }

    protected void lbLogout_Click(object sender, EventArgs e)
    {
        if (Session["idclient"] != null)
            Session.Remove("idclient");
        if (Session["idcomm"] != null)
            Session.Remove("idcomm");
        if (Session["nom"] != null)
            Session.Remove("nom");
        Response.Redirect("regions.aspx");
    }
}
