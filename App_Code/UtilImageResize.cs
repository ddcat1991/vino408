﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for UtilImageResize
/// </summary>
public class UtilImageResize
{
    public UtilImageResize()
    {
    }
        
    public static string byfactor(double factor, string fileName)
    {
        string webFilePath = HttpContext.Current.Server.MapPath(fileName);
        string webFilePath_s = HttpContext.Current.Server.MapPath("Images_s/" + fileName);
        System.Drawing.Image image = System.Drawing.Image.FromFile(webFilePath);
        System.Drawing.Image img_s = image.GetThumbnailImage((int)(image.Width * factor), (int)(image.Height * factor), null, IntPtr.Zero);
        img_s.Save(webFilePath_s, System.Drawing.Imaging.ImageFormat.Jpeg);
        img_s.Dispose();
        image.Dispose();
        return "Images_s/"+ fileName;
    }

    
}