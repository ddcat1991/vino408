﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void LoginBtn_Click(object sender, EventArgs e)
    {
        DataView dataview = (DataView)dsClient.Select(DataSourceSelectArguments.Empty);

        //user_login exist
        if (dataview.Table.Rows.Count>0)
        {
            //test_label.Text = dataview.Table.Rows[0][3].ToString();
            Session["idclient"] = dataview.Table.Rows[0][0].ToString();
            Session["nom"] = dataview.Table.Rows[0][3].ToString();

            Response.Redirect("regions.aspx");
        }
        else
        {
            //user login not exis
            test_label.Text = "the login or password is not correct"; 
        }    
       
    }
}