﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class vins : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       


    }

    protected void btnReg_Click(object sender, EventArgs e)
    {
        literalReg.Text = DropDownList1.SelectedValue;
    }



    protected void Commander_Btn_Command(object sender, CommandEventArgs e)
    {


        int Vin_Id = Int32.Parse(e.CommandArgument.ToString());
        //get the numbers of the vin
        string quantitie_str = Request.Form["Quantite" + Vin_Id];
        if (!quantitie_str.Equals(""))
        {

            //Session idcomm
            
            if (Session["idcomm"] == null)
            {
                int commNumber = 1;
                //create it in session
                dsProdcomm.SelectCommand = "SELECT max(idcomm) from [prodcomm]";
                DataView dataview = (DataView)dsProdcomm.Select(DataSourceSelectArguments.Empty);
                if (dataview.Count > 0)
                {
                    commNumber = Int32.Parse(dataview.Table.Rows[0][0].ToString());
                    commNumber++;
                    Session["idcomm"] = commNumber;
                }
            }


            int quantite = Int32.Parse(quantitie_str);
            testLable.Text = Vin_Id+" quantite is " + quantite;

            dsProdcomm.SelectCommand = "SELECT idprodcomm, idcomm, idvin, quantite FROM prodcomm WHERE idcomm="+ Session["idcomm"] + " AND idvin="+ Vin_Id ;
            IEnumerator enuma = dsProdcomm.Select(DataSourceSelectArguments.Empty).GetEnumerator();
            if (enuma.MoveNext())
            {
                //if this vin is already commend by client, the quantite will be update
                System.Data.DataRowView row = (System.Data.DataRowView)enuma.Current;
                int idprodcomm = (int)row.Row.ItemArray[0];
                int old_quantite = (int)row.Row.ItemArray[3];
                quantite += old_quantite;
                dsProdcomm.UpdateCommand = "UPDATE prodcomm SET quantite =" + quantite + "WHERE idprodcomm=" + idprodcomm;
                dsProdcomm.Update();

            }
            else {
                //if this is the first time commend this vin, insert a new line
                dsProdcomm.InsertCommand = "INSERT INTO prodcomm(idcomm, idvin, quantite) VALUES (" + Session["idcomm"] + "," + Vin_Id + "," + quantite + ")";
                dsProdcomm.Insert();
            }
        }


    }
}