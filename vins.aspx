﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="vins.aspx.cs" Inherits="vins" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Nav" Runat="Server">
    <img src="Images/LaVinotheque.gif" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Main" Runat="Server">
    <h3>Veuillez choisir la région qui vous intéresse: </h3>
        <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="dsVinRegs" DataTextField="region" DataValueField="region">
        </asp:DropDownList>
        <asp:SqlDataSource ID="dsVinRegs" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT DISTINCT [region] FROM [vins]"></asp:SqlDataSource>
        <asp:Button ID="btnReg" runat="server" Text="Voir les vins de cette région" OnClick="btnReg_Click"  />
    <hr/>
    <h2><asp:Literal ID="literalReg" runat="server"  ></asp:Literal></h2>
 
        <asp:DataList ID="DataList1" runat="server" DataSourceID="dsVins">
            <ItemTemplate>
				<table  id="vinstable" cellpadding="5">
                    <tr>
                        <td rowspan="5"><img  src='<%# Eval("nomgif") %>' width="200" height="200"/></td>                  
                        <td><asp:Label ID="nomLabel" runat="server" Text='<%# Eval("nom") %>' /></td>
                    </tr>
                    <tr>
                        <td>Millésime <asp:Label ID="Label1" runat="server" Text='<%# Eval("millesime") %>' /></td>
                    </tr>
                    <tr>
                        <td>Prix : <asp:Label ID="Label2" runat="server" Text='<%# Eval("prix") %>' /> <img src="Images/euro.gif"/></td>
                    </tr>
                    <tr>
                        <td>Quantité : <input name='Quantite<%# Eval("idvin") %>' type="number" min="1" max="100"  /></td>           
                    </tr>
                    <tr>
                        <td><asp:Button runat="server" Text="Commander"  CommandArgument='<%# Eval("idvin")   %>' OnCommand="Commander_Btn_Command" /> </td>
                    </tr>
                </table>
                <br/>
            </ItemTemplate>
        </asp:DataList>
        <asp:SqlDataSource ID="dsVins" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [vins] WHERE ([region] = @region)">
            <SelectParameters>
                <asp:ControlParameter ControlID="DropDownList1" Name="region" PropertyName="SelectedValue" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
   
    <hr />

    
    <asp:SqlDataSource ID="dsProdcomm" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" InsertCommand="INSERT INTO prodcomm(idcomm, idvin, quantite) VALUES (,,)" SelectCommand="SELECT idprodcomm, idcomm, idvin, quantite FROM prodcomm"></asp:SqlDataSource>
    
    <br/>

    <asp:Label runat="server" ID="testLable" />
</asp:Content>

