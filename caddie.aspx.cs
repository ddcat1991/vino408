﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class caddie : System.Web.UI.Page
{
    double horstotal=0;
    const double TVA = 0.196;
    const double E_TO_F = 6.55957;
    public CultureInfo cInfo {
        get;
        set;
    }

    protected void Page_Load(object sender, EventArgs e)
    {


    }

    protected void Modifier_Command(object sender, CommandEventArgs e)
    {
        int idprodcomm = Int32.Parse(e.CommandArgument.ToString());
        string quantitie_str = Request.Form["Quantite" + idprodcomm];
        
        if (!quantitie_str.Equals(""))
        {
            int new_quantite = Int32.Parse(quantitie_str);

            //delete line when quantite=0
            if (new_quantite == 0)
            {
                dsProdcomm.DeleteParameters["idprodcomm"].DefaultValue = idprodcomm+"";
                //test_label.Text = dsProdcomm.DeleteCommand;
                dsProdcomm.Delete();
            }
            //update line
            else if(new_quantite > 0)
            {
                dsProdcomm.UpdateParameters["idprodcomm"].DefaultValue = idprodcomm + "";
                dsProdcomm.UpdateParameters["quantite"].DefaultValue = quantitie_str;
                dsProdcomm.Update();
            }
        }

    }

    public double getUnitPrix(double unProdcomm)
    {
        this.horstotal += unProdcomm;
        return unProdcomm;
    }

    public double getHorsTotal()
    {
        return horstotal;
    }

    public double getTva()
    {
        return TVA * 100;
    }

    public double getTotalTva()
    {
        return TVA * this.horstotal;
    }

    public double getTotal()
    {
        double total = this.horstotal + this.getTotalTva();
        return total;
    }

    protected void Envoyer_Commande_Command(object sender, CommandEventArgs e)
    {
    }

    public void test()
    {
       
    }

    public double euroToFranc(double euro)
    {
        return euro * E_TO_F;
    }

}