﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="caddieValidate.aspx.cs" Inherits="caddieValidate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Nav" Runat="Server">
    <img src="Images/LaVinotheque.gif" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Main" Runat="Server">

    <asp:SqlDataSource ID="dsCommandes" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
        InsertCommand="INSERT INTO commandes(idclient, date, total) VALUES (@idclient, @date, @total)" 
        SelectCommand="SELECT commandes.* FROM commandes">
        <InsertParameters>
            <asp:SessionParameter Name="idclient" SessionField="idclient" />
            <asp:Parameter Name="date" />
            <asp:SessionParameter Name="total" SessionField="comm_total" />
        </InsertParameters>
    </asp:SqlDataSource>

    <asp:DataList ID="DataList1" runat="server" DataSourceID="dsProdcomm">
        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" />

        <HeaderTemplate>
            <table class="caddie">
                <tr>
                    <th></th>
                    <th></th>
                    <th>Dénomination</th>
                    <th>Quantité</th>
                    <th>Prix unitaire</th>
                    <th>Total</th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td></td>
                <td>
                    <asp:Image ID="Image1" runat="server"  ImageUrl='<%# UtilImageResize.byfactor(0.4,Eval("nomgif").ToString()) %>'/></td>
                <td>
                    <asp:Label ID="nomLabel" runat="server" Text='<%# Eval("nom") %>' /></td>
                <td>
                    <asp:Label ID="quantiteLabel" runat="server" Text='<%# Eval("quantite") %>' />
                </td>
                <td>
                    <asp:Label ID="prixLabel" runat="server" Text='<%# Eval("prix").ToString()%>' /><img src="Images/euro.gif" />
                    (<asp:Label ID="prixLabelFF" runat="server" Text='<%# euroToFranc(Convert.ToDouble(Eval("prix"))).ToString("f2")%>' />FF)
                </td>
                <td>
                    <asp:Label ID="Label1" runat="server" Text='<%# getUnitPrix(Convert.ToDouble(Eval("prix"))*Convert.ToDouble(Eval("quantite"))) %>' /><img src="Images/euro.gif" />
                     (<asp:Label ID="prixTotalLabelFF" runat="server" Text='<%# euroToFranc(Convert.ToDouble(Eval("prix"))*Convert.ToDouble(Eval("quantite"))).ToString("f2")%>' />FF)
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            <tr>
                <td colspan="2"></td>
                <td colspan="3">Total Hors Taxe</td>
                <td><%# getHorsTotal() %><img src="Images/euro.gif" />(<%# euroToFranc(getHorsTotal()).ToString("f2")%>FF)</td>
            </tr>
            <tr>
                <td colspan="2"></td>
                <td colspan="3">TVA <%# getTva() %>%</td>
                <td><%# getTotalTva().ToString("f2") %><img src="Images/euro.gif" />(<%# euroToFranc(getTotalTva()).ToString("f2")%>FF)</td>
            </tr>
            <tr>
                <td colspan="2"></td>
                <td colspan="3">Total Toutes Taxes Comprises</td>
                <td><%# getTotal().ToString("f2") %><img src="Images/euro.gif" />(<%# euroToFranc(getTotal()).ToString("f2")%>FF)</td>
            </tr>

            </table> 
        </FooterTemplate>
    </asp:DataList>

    <asp:SqlDataSource ID="dsProdcomm" runat="server"
        ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        SelectCommand="SELECT vins.nomgif, vins.nom, prodcomm.quantite, vins.prix, prodcomm.idprodcomm, vins.prix*prodcomm.quantite AS prodcomm_total FROM vins INNER JOIN prodcomm ON vins.idvin = prodcomm.idvin WHERE (prodcomm.idcomm = @idcomm)"
        DeleteCommand="DELETE FROM prodcomm WHERE (idprodcomm = @idprodcomm)"
        UpdateCommand="UPDATE prodcomm SET quantite = @quantite WHERE (idprodcomm = @idprodcomm)">
        <DeleteParameters>
            <asp:Parameter Name="idprodcomm" DefaultValue="0" />
        </DeleteParameters>
        <SelectParameters>
            <asp:SessionParameter DefaultValue="0" Name="idcomm" SessionField="idcomm" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="quantite" />
            <asp:Parameter Name="idprodcomm" />
        </UpdateParameters>
    </asp:SqlDataSource>

    <br />
    <asp:Button ID="Valide_Btn" runat="server" Text="Valider" OnClick="Validate_Btn_Click" />

    <h2>
    <asp:Label  Visible="false" runat="server" ID="validationInfo" Text="Votre commande a été transmise à votre service de ventes par correpondance. Nous vous remercions pour votre confiance !"></asp:Label>
    </h2>

</asp:Content>

